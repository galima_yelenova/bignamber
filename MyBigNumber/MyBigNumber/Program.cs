﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBigNumber
{
    struct BigNumber
    {
        public int[] a;
        public int size;
        public BigNumber(string s)
        {
            a = new int[1000];
            size = s.Length - 1;
            for (int i = size, j = 0; i >= 0; i--, j++)
            {
                a[j] = s[i] - '0';
            } 
        }
        public override string ToString()
        {
            string s = "";
            for (int i = size; i >= 0; i--)
            {
                s += a[i].ToString();
            }
                return s;
        }
        public static BigNumber operator +(BigNumber x, BigNumber y)
        {
            int k = 0;
            int sz;
            if (x.size > y.size) sz = x.size;
            else sz = y.size;
            for (int i = sz; i >= 0; i--)
            {
                x.a[i] += y.a[i] + k;
                k = x.a[i] / 10;
                x.a[i] %= 10;
            }
            return x; 

        }
            
        }
    
    class Program
    {
        static void Main(string[] args)
        {
            string s1 = Console.ReadLine();
            string s2 = Console.ReadLine();
            BigNumber x = new BigNumber(s1);
            BigNumber y = new BigNumber(s2);
            BigNumber z = new BigNumber();
            z = x + y;
            Console.WriteLine(z);
            Console.ReadKey(); 
        }
    }
}
